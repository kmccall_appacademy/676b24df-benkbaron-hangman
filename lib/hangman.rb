require 'byebug'

class Hangman

  def initialize (players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  attr_reader :guesser, :referee, :secret_length
  attr_accessor :board

  def setup
    @secret_length = referee.pick_secret_word
    self.board = Array.new(secret_length)
    guesser.register_secret_length(secret_length)
  end

  def take_turn
    ltr_guess = guesser.guess(board)
    hits = referee.check_guess(ltr_guess)
    update_board(ltr_guess, hits)
    guesser.handle_response(ltr_guess, hits)
  end

  def update_board (ltr_guess, hits)
    board.each_with_index do |ltr, idx|
      if hits.include?(idx)
        board[idx] = ltr_guess
      end
    end
  end

  def display
    puts
    board.each do |el|
      if el == nil
        print " _ "
      else
        print el
      end
    end
    puts
  end

  def play
    setup
    until board.include?(nil) == false
      display
      take_turn
    end
    display
  end

end

class HumanPlayer

  def initialize (dictionary)
    @guessed = []
    @candidate_words = dictionary.map! {|word| word.chomp}
  end

  attr_accessor :secret_word, :guessed, :candidate_words

  def pick_secret_word
    @secret_word = gets.chomp
    secret_word.length
  end

  def check_guess (ltr_guess)
    hits = []
    secret_word.chars.each_with_index do |ltr, idx|
      hits << idx if ltr == ltr_guess
    end
    hits
  end

  def handle_response (ltr, hits)
    candidate_words.select! {|word| word_match?(word, ltr, hits)}
  end

  def word_match?(word, ltr, hits)
    word_hits = []
    word.chars.each_with_index {|char, idx| word_hits << idx if char == ltr}
    word_hits == hits
  end


  def register_secret_length(word_length)
    candidate_words.select! {|word| word.length == word_length}
  end

  def guess (board)
    gets.chomp
    # big_str = candidate_words.join
    # most_common = nil
    # ("a".."z").each do |ltr|
    #   next if board.include?(ltr) || guessed.include?(ltr)
    #   if most_common == nil || big_str.count(ltr) > big_str.count(most_common)
    #     most_common = ltr
    #   end
    # end
    # guessed << most_common
    # most_common
  end

end

class ComputerPlayer

  def initialize (dictionary)
    @guessed = []
    @candidate_words = dictionary.map! {|word| word.chomp}
  end

  attr_accessor :secret_word, :guessed, :candidate_words

  def pick_secret_word
    @secret_word = candidate_words.sample
    secret_word.length
  end

  def check_guess (ltr_guess)
    hits = []
    secret_word.chars.each_with_index do |ltr, idx|
      hits << idx if ltr == ltr_guess
    end
    hits
  end

  def handle_response (ltr, hits)
    candidate_words.select! {|word| word_match?(word, ltr, hits)}
  end

  def word_match?(word, ltr, hits)
    word_hits = []
    word.chars.each_with_index {|char, idx| word_hits << idx if char == ltr}
    word_hits == hits
  end


  def register_secret_length(word_length)
    candidate_words.select! {|word| word.length == word_length}
  end

  def guess (board)
    sleep(1)
    big_str = candidate_words.join
    most_common = nil
    ("a".."z").each do |ltr|
      next if board.include?(ltr) || guessed.include?(ltr)
      if most_common == nil || big_str.count(ltr) > big_str.count(most_common)
        most_common = ltr
      end
    end
    guessed << most_common
    most_common
  end
end
# 
# ben = HumanPlayer.new(File.readlines("dictionary.txt"))
# compy = ComputerPlayer.new(File.readlines("dictionary.txt"))
#
# game = Hangman.new(guesser: ben, referee: compy)
#
# game.play
